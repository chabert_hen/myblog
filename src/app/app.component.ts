import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'myBlog';
  posts = [
    new Post(
      'First post',
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit.' +
        'Duis vel elit fermentum, malesuada dui ac, euismod nibh.' +
        'Sed nisl nulla, placerat a justo in, interdum maximus magna.'
    ),
    new Post(
      'Second post',
      'In hac habitasse platea dictumst. Nullam gravida eget nunc vel hendrerit. Suspendisse feugiat porttitor gravida.' +
        'Interdum et malesuada fames ac ante ipsum primis in faucibus.' +
        'Aliquam et turpis sit amet urna placerat molestie. Aenean orci orci, faucibus quis ultricies eu, placerat nec metus.'
    ),
    new Post(
      'Third post',
      'Fusce ac tellus eu mi faucibus tempus ut nec arcu. Nunc molestie laoreet hendrerit.' +
        'In ullamcorper sollicitudin ante, sit amet volutpat arcu suscipit volutpat.' +
        'Suspendisse pretium tincidunt magna, eu pretium arcu. Quisque at neque eu nibh volutpat lobortis.'
    ),
    new Post(
      'Fourth post',
      'Ut dui purus, sollicitudin congue felis facilisis, rhoncus pulvinar justo.' +
        'Nullam dignissim, elit ac vulputate fringilla, risus magna suscipit velit,' +
        'sit amet elementum nunc libero eget dolor. Fusce maximus nisi eu nisi blandit, sit amet venenatis velit imperdiet.'
    ),
    new Post(
      'Fifth post',
      'Donec pretium, nibh id sollicitudin laoreet, nibh sapien posuere justo, sed malesuada mi ante non enim. ' +
        'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.' +
        'Cras at scelerisque tortor. Donec vel urna ac elit dictum efficitur.'
    ),
  ];
}

export class Post {
  title: string;
  content: string;
  createdAt: Date;
  nbLikes: number;

  constructor(title, content) {
    this.title = title;
    this.content = content;
    this.createdAt = new Date();
    this.nbLikes = 0;
  }
}
