import { Component, OnInit, Input } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() title: string;

  constructor() { }

  ngOnInit() {
    $(document).ready(() => {
      $('#myInput').on('keyup', function() {
        const value = $(this).val().toLowerCase();
        $('#myPosts app-post-list-item').filter(function() {
          $(this).toggle($(this).find('.card-title').text().toLowerCase().indexOf(value) > -1);
        });
      });
    });
  }

}
